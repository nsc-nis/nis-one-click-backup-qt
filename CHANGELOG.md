## Version 1.2.2
Initial Flatpak release

## Version 1.2.1
This is a small bugfix update for the Linux AppImage-Version
- fixed FolderDialog not opening

## Version 1.2-AppImage 
Added AppImage for Linux-Users

## Version 1.2
- Fixed application not creating folder for backup
- Fixed windows-installer not creating start menu entry for non-admin users
- Added creation of desktop-shortcuts to windows-installer
- Fixed uninstaller not removing all the files

## Version 1.1
- Added Windows installer

## Version 1.0
- First stable release
