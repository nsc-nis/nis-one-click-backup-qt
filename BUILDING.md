# Windows (Installer)
## 1. Building the application
Download Qt and the IDE Qt Creator, open the projects .pro-file and click on "Build". Copy the contents of the build directory to a new directory. Create a new folder inside this directory and copy all the .qml-files there.
After that, run the PowerShell-Script "deploy.ps1" locatet under packaging/windows-installer of this repository and give it the directory to the .qml-files as well as the directory where you copied your build output.
For example .\deploy.ps1 C:\deploy\qmldir C:\deploy. After that, you can execute the .exe-file in this directory and run the app.

## 2. Making an installer with NSIS (optional)
Download and install the Nullsoft Scriptable Installer System (NSIS). Copy the file "Modern.nsh" locatet under packaging/windows-installer of this repository to C:\Program Files (x86)\NSIS\Contrib\zip2exe. Next, make a .ZIP-file of the folder you ran the deploy-script in. After that, open the NSIS application and click on "Installer based on .ZIP file".
For the source ZIP file, select the file you just created from your deploy-directory. For the installer name, choose "NIS One-Click-Backup", for the interface choose "Modern". For the default folder, choose "$PROGRAMFILES\NIS\One-Click-Backup". Leave the rest of the options on the default selection. For the output EXE file, you can select a path of your choice. Finally, click "Generate" which will create the installer executeable.

# Linux
## 1. Building the application
First, install Qt6 (preferably Qt 6.3.0) for examle with [aqt](https://github.com/miurahr/aqtinstall). Then clone the Git-repository and make a new folder for your build. Inside the folder first, execute `qmake ../src/NIS_One-Click-Backup_Qt.pro`. **Attention**: Be sure to use the Qt6 version, otherwise it won't work. After that, run `make` which will generate the binary.
