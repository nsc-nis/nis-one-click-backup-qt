#include "backupplan.h"

BackupPlan::BackupPlan()
{

}

BackupPlan::BackupPlan(QString os, QString destination, QList<QString> directories)
{
    this->os = os;
    this->destination = destination;
    this->directories = directories;
}
void BackupPlan::execute()
{
    if(os != "UNSUPPORTED")
    {
        /*
          Calculate steps for progress bar
        */
        progressSteps = 1.0 / ((double) directories.length() + 1.0);
        qDebug() << "[ DEBUG ] directories.length(): " << directories.length();
        qDebug() << "[ DEBUG ] progressSteps: " << progressSteps;
        progress = 0;

        // Why does it always crash on Windows if you backup exactly 4 folders?
        if(progressSteps == 0.2)
        {
            progressSteps = 1.0 / (double) directories.length();
        }

        /*
          create the backup directory with the date in the folder-name
        */
        QDate date = QDate::currentDate();  //get current date

        //convert current date as QString to char*
        char* destinationChar;
        //std::string destinationString = destination.toStdString();
        destinationChar = new char [255];
        //strcpy(destinationChar, destinationString.c_str());

        //format the destination folder name
        qDebug() << "[ DEBUG ] destination (root): " << destination;
        qDebug() << "[ DEBUG ] destinationChar: " << destinationChar;
        //sprintf(destinationChar, "%s/Backup_%02d-%02d-%d", destinationChar, date.day(), date.month(), date.year());
        sprintf(destinationChar, "/Backup_%02d-%02d-%d", date.day(), date.month(), date.year());
        //destination = destination + "/Backup_" + QString::number(date.day()) + "-" + QString::number(date.month()) + "-" + QString::number(date.year());
        qDebug() << "[ DEBUG ] destinationChar: " << destinationChar;

        //convert formatted char* to QString
        int charLen = strlen(destinationChar);
        //destination = QString::fromUtf8((const char*)destinationChar, charLen);
        destination.append(QString::fromUtf8((const char*)destinationChar, charLen));
        qDebug() << "[ DEBUG ] Destination fixed...?: " << destination;

        //create the destination folder and trigger the progressBar update
        std::filesystem::create_directory(destination.toStdString());
        backupThreadFinished();

        /*
          Run the backup-threads
        */
        for(int i = 0; i < directories.length(); ++i)
        {
            qDebug() << "[ *** ] Backing up: " << directories.at(i);
            QString name = getDirectoryName(directories.at(i)); //get the name of the folder to backup without it's path, e.g. "Downloads"
            //qDebug() << "[ DEBUG ] Directory name: " << name;

            QString targetPath = destination + "/" + name;          //path where the files of the backup are going to be stored
            //qDebug() << "[ OK ] Target directory " << targetPath;
            std::string target = targetPath.toStdString();

            QThread* thread = new QThread();
            //Worker* worker = new Worker(directories.at(i).toStdString(), target);
            Worker* worker = new Worker(directories.at(i).toStdString(), target, os);
            worker->moveToThread(thread);
            connect( worker, &Worker::error, this, &BackupPlan::errorString);
            connect( thread, &QThread::started, worker, &Worker::process);
            connect( worker, &Worker::finished, this, &BackupPlan::backupThreadFinished);
            connect( worker, &Worker::finished, thread, &QThread::quit);
            connect( worker, &Worker::finished, worker, &Worker::deleteLater);
            connect( thread, &QThread::finished, thread, &QThread::deleteLater);
            thread->start();
        }
    }
}

void BackupPlan::setDestination(QString path)
{
    destination = path;
}

QString BackupPlan::getDestination() const
{
    return destination;
}

void BackupPlan::addDirectory(QString path)
{
    directories.append(path);
}

QList<QString> BackupPlan::getDirectories() const
{
    return directories;
}

void BackupPlan::removeDirectory(QString path)
{
    for(int i = 0; i < directories.length(); ++i)
    {
        int match = QString::compare(path, directories.at(i), Qt::CaseInsensitive);
        qDebug() << "[ DEBUG ] Path matches directory entry: " << match;
        if(match == 0)
        {
            directories.removeAt(i);
        }
    }
}

void BackupPlan::setOS(QString ostype)
{
    os = ostype;
}

QString BackupPlan::getOS() const
{
    return os;
}

void BackupPlan::setDirectories(QList<QString> list)
{
    directories = list;
}

QString BackupPlan::getDirectoryName(QString path)
{
    const std::string strPath = path.toStdString();
    std::vector<const char*> vecName = split(strPath, '/');
    const char* charName = vecName.at(vecName.size() - 1);
    std::string str(charName);
    return QString::fromStdString(str);
}

std::vector<const char*> BackupPlan::split(const std::string &s, char delim)
{
    std::vector<const char*> elems;
    std::stringstream ss(s);
    std::string number;
    while(std::getline(ss, number, delim))
    {
        elems.push_back(number.c_str());
    }
    return elems;
}

void BackupPlan::backupThreadFinished()
{
    progress = progress + progressSteps;
    qDebug() << "[ OK ] Backup done: " << (progress * 100) << "%";

    emit backupProgress(progress);
}

void BackupPlan::errorString(QString err)
{
    emit errorMessage(err);
    qDebug() << "[ ERROR ] Backup-Thread returned an error: " << err;
}

void BackupPlan::reset()
{
    destination = "";
    directories.clear();
}


QDataStream &operator<<(QDataStream &out, const BackupPlan &backupPlan)
{
    out << backupPlan.getOS() << backupPlan.getDestination() << backupPlan.getDirectories();
    return out;
}

QDataStream &operator>>(QDataStream &in, BackupPlan &backupPlan)
{
    QString os;
    QString destination;
    QList<QString> directories;
    in >> os >> destination >> directories;
    backupPlan.setOS(os);
    backupPlan.setDestination(destination);
    backupPlan.setDirectories(directories);
    return in;
}

