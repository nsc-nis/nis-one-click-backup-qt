import QtQuick 2.0
import QtQuick.Controls 2.15

/*
  Show an About-dialog with Information about the program
*/
Window
{
    id: window_aboutWindow
    width: 300
    height: 400
    title: qsTr("About NIS One-Click-Backup")
    visible: true

    Rectangle
    {
        id: image_logo
        height: 159
        width: 100
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter

        Image
        {
            height: parent.height
            width: parent.width
            source: "qrc:/resources/NIS Logo1.png"
        }
    }

    Label
    {
        id: text_programName
        text: "NIS One-Click-Backup"
        anchors.top: image_logo.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 18
        font.bold: true
    }

    Label
    {
        id: text_version
        text: qsTr("Version: ") + "1.2.2"
        anchors.top: text_programName.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 14
    }

    Label
    {
        id: text_author
        text: qsTr("2022 - 2024 NSC IT Solutions")
        anchors.top: text_version.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 14
    }

    Text
    {
        id: text_issues
        width: parent.width - 20
        text: qsTr("Please report any issues on ") + "<a href='https://gitlab.com/dev-nis/nis-one-click-backup-qt'>GitLab</a>"
        anchors.top: text_author.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 14
        wrapMode: Text.WordWrap
        onLinkActivated: Qt.openUrlExternally("https://gitlab.com/nsc-nis/nis-one-click-backup-qt")
    }

    Button
    {
        id: button_close
        height: 40
        text: qsTr("Close")
        onClicked: window_aboutWindow.close()
        anchors.top: text_issues.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        icon.source: "qrc:/resources/NIS_OCB_icon_delete.png"
        icon.color: "transparent"
    }
}
