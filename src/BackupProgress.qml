import QtQuick 2.0
import QtQuick.Controls 2.5

Window
{
    id: window_progress
    width: 690
    height: 100
    visible: false

    title: qsTr("Backup in progress, please wait...")

    Connections
    {
        target: backupManager
        function onBackupManagerProgress(progress)
        {
            progressBar.value = progress;
        }
        function onBackupStarted()
        {
            visible = true;
        }
    }

    Label
    {
        id: label_progressTitle
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        width: parent.width
        //anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Your backup is in progress, please wait...")
    }

    Label
    {
        id: label_progress
        anchors.top: label_progressTitle.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
        text: (progressBar.value * 100) + "% " + qsTr("done")
    }

    ProgressBar
    {
        id: progressBar
        anchors.top: label_progress.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        //value: 0.5
    }

    Button
    {
        id: button_close
        text: qsTr("Close")
        anchors.top: progressBar.bottom
        anchors.topMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        enabled: false
        onClicked: {
            window_progress.active = false;
            window_progress.visible = false;
        }
    }
}
