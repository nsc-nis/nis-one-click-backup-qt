#include "worker.h"

Worker::Worker(std::string source, std::string destination, QString os)
{
    this->destination = destination;
    this->source = source;
    this->os = os;
}

Worker::~Worker()   // Destructor
{
    // free resources
}

void Worker::process()  // Process. Start processing data.
{
    qDebug() << "[ *** ][ BackupThread ] Running backup thread " << QString::fromStdString(source) << " : " << QString::fromStdString(destination);
    //std::filesystem::create_directories(destination);            //create the backup directory

    try
    {
        std::filesystem::copy(source, destination, std::filesystem::copy_options::recursive | std::filesystem::copy_options::overwrite_existing);  //copy the files
    }
    catch(const std::filesystem::__cxx11::filesystem_error& exception)
    {
        std::string systemCMD;
        if(os == "WINDOWS")
        {
            systemCMD = "xcopy \"" + source + "\" \"" + destination + "\" /i /s /e /y";
        }
        else if(os == "LINUX")
        {
            systemCMD = "cp -r -v '" + source + "' '" + destination  + "'";
        }

        qDebug() << "[ WARN ][ BackupThread ] C++ copy failed, trying fallback: " << QString::fromStdString(systemCMD);
        int err = system(systemCMD.c_str());

        if(err != 0)
        {
            qDebug() << "[ ERROR ][ BackupThread ] Error while copying " << QString::fromStdString(source) << ": " << exception.what();
            std::string msg = exception.what();
            emit error(QString::fromStdString("Error while copying " + source + ": " + msg));
        }
        else
        {
            qDebug() << "[ OK ][ BackupThread ] Fallback version executed successfully";
        }
    }
    qDebug() << "[ OK ][ BackupThread ] Finished";

    emit finished();
}
