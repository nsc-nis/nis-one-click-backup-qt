import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

/*
  Show a window that displays help information
*/
Window
{
    id: window_helpWindow
    width: 420
    height: 310
    title: qsTr("How to use NIS One-Click-Backup")
    visible: true

    RowLayout
    {
        id: layout_title
        width: parent.width
        Layout.alignment: Qt.AlignHCenter + Qt.AlignTop

        /*
          Logo of the application
          */
        Rectangle
        {
            id: image_logo
            height: 64
            width: 64
            Layout.leftMargin: 10
            Layout.rightMargin: 0
            Layout.alignment: Qt.AlignRight

            Image
            {
                height: parent.height
                width: parent.width
                source: "qrc:/resources/NIS_OCB_Icon_help.png"
            }
        }

        /*
          Title of the application
          */
        Label
        {
            id: label_title
            text: "How to use?"
            font.pointSize: 42
            color: "blue"
            Layout.alignment: Qt.AlignLeft
        }
    }

    Text
    {
        id: text_helpDocumentation
        text: qsTr("One-Click-Backup saves (copies) your folders to an external location of your choice. You can select the default folders in your home directory as well as add aditional folders with the button 'Choose Folder' at the bottom right")
        wrapMode: Text.WordWrap
        anchors.top: layout_title.bottom
        font.pointSize: 14
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Button
    {
        id: button_close
        height: 40
        text: qsTr("Close")
        onClicked: window_helpWindow.close()
        anchors.top: text_helpDocumentation.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        icon.source: "qrc:/resources/NIS_OCB_icon_delete.png"
        icon.color: "transparent"
    }
}
