QT += core gui quick widgets

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        backupmanager.cpp \
        backupplan.cpp \
        main.cpp \
        worker.cpp

RESOURCES += qml.qrc

TRANSLATIONS += \
    NIS_One-Click-Backup_Qt_de_DE.ts
CONFIG += lrelease
CONFIG += embed_translations

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# Icons
RC_ICONS = resources/NIS_OCB_Icon_logo.ico
ICON = resources/NIS_OCB_Icon_logo.icns

HEADERS += \
    backupmanager.h \
    backupplan.h \
    worker.h
