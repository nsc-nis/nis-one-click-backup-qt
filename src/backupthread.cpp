#include "backupthread.h"

BackupThread::BackupThread(std::string source, std::string destination)
{
    this->destination = destination;
    this->source = source;
}

/*
void BackupThread::run()
{
    qDebug() << "[ *** ][ BackupThread ] Running backup thread";
    std::filesystem::create_directories(destination);            //create the backup directory
    std::filesystem::copy(source, destination, std::filesystem::copy_options::overwrite_existing | std::filesystem::copy_options::recursive);  //copy the files
    qDebug() << "[ OK ][ BackupThread ] Finished";
    exec();
}
*/

void BackupThread::run()
{
    /* ... here is the expensive or blocking operation ... */
    qDebug() << "[ *** ][ BackupThread ] Running backup thread";
    std::filesystem::create_directories(destination);            //create the backup directory
    std::filesystem::copy(source, destination, std::filesystem::copy_options::overwrite_existing | std::filesystem::copy_options::recursive);  //copy the files
    qDebug() << "[ OK ][ BackupThread ] Finished";

    emit resultReady();
}
