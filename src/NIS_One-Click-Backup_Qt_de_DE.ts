<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="AboutWindow.qml" line="12"/>
        <source>About NIS One-Click-Backup</source>
        <translation>Über NIS One-Click-Backup</translation>
    </message>
    <message>
        <location filename="AboutWindow.qml" line="45"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="AboutWindow.qml" line="54"/>
        <source>2022 - 2023 NSC IT Solutions</source>
        <translation>2022 - 2023 NSC IT Solutions</translation>
    </message>
    <message>
        <location filename="AboutWindow.qml" line="64"/>
        <source>Please report any issues on </source>
        <translation>Bitte melden Sie Fehler auf </translation>
    </message>
    <message>
        <location filename="AboutWindow.qml" line="76"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>CustomFolderList</name>
    <message>
        <location filename="CustomFolderList.qml" line="34"/>
        <source>Choose an additional folder you want to backup</source>
        <translation>Wähle einen zusätzlichen Ordner zum sichern</translation>
    </message>
    <message>
        <location filename="CustomFolderList.qml" line="42"/>
        <source>Choose Folder</source>
        <translation>Ordner auswählen</translation>
    </message>
</context>
<context>
    <name>HelpWindow</name>
    <message>
        <location filename="HelpWindow.qml" line="13"/>
        <source>How to use NIS One-Click-Backup</source>
        <translation>Wie man NIS One-Click-Backup benutzt</translation>
    </message>
    <message>
        <location filename="HelpWindow.qml" line="58"/>
        <source>One-Click-Backup saves (copies) your folders to an external location of your choice. You can select the default folders in your home directory as well as add aditional folders with the button &apos;Choose Folder&apos; at the bottom right</source>
        <translation>One-Click-Backup speichert (kopien von) Ihren Ordnern auf einem externen Ort Ihrer Wahl. Sie können die Standard-Ordner in Ihrem Benutzerverzeichnis auswählen und zusätzliche Ordner mit einem Klick auf &quot;Ordner auswählen&quot; unten rechts hinzufügen</translation>
    </message>
    <message>
        <location filename="HelpWindow.qml" line="73"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>LanguageWindow</name>
    <message>
        <location filename="LanguageWindow.qml" line="13"/>
        <source>Change user interface language</source>
        <translation>Sprache der Oberfläche ändern</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="16"/>
        <source>NIS One-Click-Backup</source>
        <translation>NIS One-Click-Backup</translation>
    </message>
    <message>
        <location filename="main.qml" line="45"/>
        <source>Backup finished! You can close this dialog now</source>
        <translation>Sicherung abgeschlossen! Sie können diesen Dialog jetzt schließen</translation>
    </message>
    <message>
        <location filename="main.qml" line="76"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="main.qml" line="83"/>
        <source>Save current backup-plan</source>
        <translation>Den aktuellen Sicherungsplan speichern</translation>
    </message>
    <message>
        <location filename="main.qml" line="92"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="main.qml" line="99"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="main.qml" line="107"/>
        <source>Language settings</source>
        <translation>Spracheinstellungen</translation>
    </message>
    <message>
        <location filename="main.qml" line="120"/>
        <source>Export backup plan</source>
        <translation>Sicherungsplan exportieren</translation>
    </message>
    <message>
        <location filename="main.qml" line="128"/>
        <source>Import backup plan</source>
        <translation>Sicherungsplan importieren</translation>
    </message>
    <message>
        <location filename="main.qml" line="133"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="main.qml" line="140"/>
        <source>How to use One-Click-Backup</source>
        <translation>Wie man NIS One-Click-Backup benutzt</translation>
    </message>
    <message>
        <location filename="main.qml" line="153"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="main.qml" line="234"/>
        <source>Choose a place where you want your backup to be stored</source>
        <translation>Wählen Sie einen Ort an dem Ihre Sicherung gespeichert werden soll</translation>
    </message>
    <message>
        <location filename="main.qml" line="255"/>
        <source>Choose Destination</source>
        <translation>Ziel auswählen</translation>
    </message>
    <message>
        <location filename="main.qml" line="277"/>
        <source>User folders to backup</source>
        <translation>Benutzer-Ordner welche gesichert werden sollen</translation>
    </message>
    <message>
        <location filename="main.qml" line="288"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="main.qml" line="295"/>
        <source>Documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="main.qml" line="302"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="main.qml" line="309"/>
        <source>Desktop</source>
        <translation>Desktop/Arbeitsfläche/Schreibtisch</translation>
    </message>
    <message>
        <location filename="main.qml" line="316"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="main.qml" line="323"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="main.qml" line="330"/>
        <source>Additional folders to backup</source>
        <translation>Zusätzliche Ordner welche gesichert werden sollen</translation>
    </message>
    <message>
        <location filename="main.qml" line="420"/>
        <source>Choose an additional folder you want to backup</source>
        <translation>Wählen Sie einen zusätzlichen Ordner der gesichert werden soll</translation>
    </message>
    <message>
        <location filename="main.qml" line="451"/>
        <source>Choose Folder</source>
        <translation>Ordner auswählen</translation>
    </message>
    <message>
        <location filename="main.qml" line="512"/>
        <source>Start Backup</source>
        <translation>Sicherung starten</translation>
    </message>
    <message>
        <location filename="main.qml" line="613"/>
        <source>Your backup is in progress, please wait...</source>
        <translation>Ihre Sicherung wird erstellt, bitte warten...</translation>
    </message>
    <message>
        <location filename="main.qml" line="624"/>
        <source>done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="main.qml" line="643"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
</TS>
