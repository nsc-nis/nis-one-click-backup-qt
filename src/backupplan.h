#ifndef BACKUPPLAN_H
#define BACKUPPLAN_H

#include <QObject>
#include <QDebug>
#include <QDate>
#include <filesystem>
#include "backupthread.h"
#include "worker.h"

class BackupPlan : public QObject
{
    Q_OBJECT
public:
    BackupPlan();
    BackupPlan(QString, QString, QList<QString>);
    //~BackupPlan();

signals:
    void backupProgress(double progress);
    void errorMessage(QString message);

public:
    void execute();
    void addDirectory(QString);
    void setDestination(QString);
    void setDirectories(QList<QString>);
    QString getDestination() const;
    void setOS(QString);
    QString getOS() const;
    QList<QString> getDirectories() const;
    void removeDirectory(QString);
    void reset();
private:
    QString os;
    QString destination;
    QList<QString> directories;
    QString getDirectoryName(QString);
    std::vector<const char*> split(const std::string &s, char delim);
    double progress;
    double progressSteps;

/*
private slots:
    void startBackup();
*/
private slots:
    void backupThreadFinished();
    void errorString(QString);

};
QDataStream &operator<<(QDataStream &out, const BackupPlan &backupPlan);
QDataStream &operator>>(QDataStream &in, BackupPlan &backupPlan);

#endif // BACKUPPLAN_H
