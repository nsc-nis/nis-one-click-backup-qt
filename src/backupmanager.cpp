#include "backupmanager.h"

#ifdef Q_OS_LINUX
  //std::cout << "Linux version";
   #define OCB_OS "LINUX";
#elif defined(Q_OS_WIN32)
   #define OCB_OS "WINDOWS";
#elif defined(Q_OS_MAC)
    #define OCB_OS "MAC";
#else
    #define OCB_OS "UNSUPPORTED"
#endif

BackupManager::BackupManager(QObject *parent)
    : QObject{parent}
{
    os = OCB_OS;
    qDebug() << "[ INFO ] Operating system is " << os;

    if(os == "UNSUPPORTED")
    {
        emit backupManagerError("ERROR: Your operating system is not supported by NIS One-Click-Backup");
    }

    //set OS-type
    backupPlan.setOS(os);

    username = qgetenv("USER");
    if (username.isEmpty())
    {
        username = qgetenv("USERNAME");
    }
    qDebug() << "[ INFO ] Current user is: " << username;

    //Add standard paths
    backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).at(0));
    backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0));
    backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::MusicLocation).at(0));
    backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).at(0));

    QObject::connect(&backupPlan, SIGNAL(backupProgress(double)), this, SLOT(onBackuProgressChanged(double)));
    QObject::connect(&backupPlan, SIGNAL(errorMessage(QString)), this, SLOT(onErrorMessage(QString)));

    importPlan("import.nscf");
}

void BackupManager::setDestination(QString path)
{

    if(os == "LINUX")
    {
        path = "/" + path;
    }
    qDebug() << "[ INFO ] Destination path is " << path;
    destinationPath = path;
    backupPlan.setDestination(path);
}

QString BackupManager::getDestination()
{
    return destinationPath;
}

void BackupManager::startBackup()
{
   emit backupStarted();

   backupPlan.execute();
}

void BackupManager::receiveDestination(QString path)
{
    setDestination(path);
    emit enableStartButton();
}

void BackupManager::receiveAdditional(QString path)
{
    if(os == "LINUX")
    {
        path = "/" + path;
    }
    qDebug() << "[ OK ] Additional path set: " << path;

    backupPlan.addDirectory(path);
}

void BackupManager::onDefaultFolderChanged(QString path, bool checked)
{
    if(checked)
    {
        if(path == "Pictures")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).at(0));
        }
        else if(path == "Documents")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0));
        }
        else if(path == "Downloads")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).at(0));
        }
        else if(path == "Desktop")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0));
        }
        else if(path == "Music")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::MusicLocation).at(0));
        }
        else if(path == "Videos")
        {
            backupPlan.addDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).at(0));
        }
    }
    else
    {
        if(path == "Pictures")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).at(0));
        }
        else if(path == "Documents")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0));
        }
        else if(path == "Downloads")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).at(0));
        }
        else if(path == "Desktop")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0));
        }
        else if(path == "Music")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::MusicLocation).at(0));
        }
        else if(path == "Videos")
        {
            backupPlan.removeDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).at(0));
        }
    }
}

void BackupManager::onBackuProgressChanged(double progress)
{
    //qDebug() << "[ OK ][ BACKUPMANAGER ] Progress received: " << (progress * 100);
    emit backupManagerProgress(progress);
}

void BackupManager::onAdditionalFolderDeleted(QString path)
{
    if(os == "LINUX")
    {
        path = "/" + path;
    }
    backupPlan.removeDirectory(path);
}

void BackupManager::onErrorMessage(QString message)
{
    emit backupManagerError(message);
}

void BackupManager::exportPlan(QString path)
{
    if(os == "LINUX")
    {
        path = "/" + path;
    }
    qDebug() << "[ OK ] Received export path: " << path;
    path = path + "/backupplan.nscf";

    //Save objects
    QFile *outputFile = new QFile(path);
    outputFile->open(QIODevice::WriteOnly);

    if(!outputFile->isOpen())
    {
        emit backupManagerError("Couldn't export Backup-plan: couldn't create and open file");
    }
    else
    {
        QDataStream dataStream(outputFile);
        dataStream << backupPlan;
        outputFile->close();

        emit backupManagerError("BackupPlan successfully exported to " + path);
    }
}

void BackupManager::importPlan(QString path)
{
    if(os == "LINUX")
    {
        path = "/" + path;
    }
    qDebug() << "[ OK ] Received import path: " << path;

    //Load objects
    QFile inputFile(path);
    inputFile.open(QIODevice::ReadOnly);

    if(!inputFile.isOpen())
    {
        emit backupManagerError("Couldn't import backup-plan: couldn't open file");
    }
    else
    {
        QDataStream dataStream(&inputFile);
        //content2 = dataStream.readAll();
        dataStream >> backupPlan;

        inputFile.close();

        //check if OS matches
        if(backupPlan.getOS() != os)
        {
            QString wrongOS = backupPlan.getOS();
            backupPlan.reset();
            backupPlan.setOS(os);
            emit backupManagerError("Couldn't import backup-plan: import file has a different operating system than the current one: " + wrongOS + " != " + os);
        }
        else
        {
            //get default folders for UI checkboxes
            QList<QString> additionals;
            bool defaultFolders[6] = {false, false, false, false, false, false};
            for(QString folder : backupPlan.getDirectories())
            {
                if(folder == QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).at(0))
                {
                    defaultFolders[0] = true;
                }
                else if(folder == QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0))
                {
                    defaultFolders[1] = true;
                }
                else if(folder == QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).at(0))
                {
                    defaultFolders[2] = true;
                }
                else if(folder == QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0))
                {
                    defaultFolders[3] = true;
                }
                else if(folder == QStandardPaths::standardLocations(QStandardPaths::MusicLocation).at(0))
                {
                    defaultFolders[4] = true;
                }
                else if(folder == QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).at(0))
                {
                    defaultFolders[5] = true;
                }
                else
                {
                    additionals.append(folder);
                }
            }

            if(os == "LINUX")
            {
                QString wrongPath = backupPlan.getDestination();
                QStringList arr = wrongPath.split("/");
                QString path;
                for(int i = 1; i < arr.length(); ++i)
                {
                    path += arr.at(i);
                    if((i + 1) < arr.length())
                    {
                        path += "/";
                    }
                }
                backupPlan.setDestination(path);
            }

            //convert QList to array
            QString additionalFolders;
            for(int i = 0; i < additionals.size(); ++i)
            {
                additionalFolders += additionals.at(i);
                additionalFolders += ",";
            }

            //convert bool to csv-string
            QString defaultFoldersString;
            for(int i = 0; i < sizeof(defaultFolders); ++i)
            {
                if(defaultFolders[i])
                {
                    defaultFoldersString += "true,";
                }
                else
                {
                    defaultFoldersString += "false,";
                }
            }

            qDebug() << "[ DEBUG ] CSV signal string: " << backupPlan.getDestination() << ";" << defaultFoldersString << ";" << additionalFolders;

            //emit initSettings(backupPlan.getDestination(), defaultFolders, additionalFolders); ==> didn't work unfortunately, no arrays arrived in QML
            emit initSettings(backupPlan.getDestination() + ";" + defaultFoldersString + ";" + additionalFolders);
            emit backupManagerError("BackupPlan imported successfully");
        }
    }
}
