#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <filesystem>
#include <QDebug>

class Worker : public QObject
{
    Q_OBJECT
public:
    Worker(std::string, std::string, QString);
    ~Worker();
public slots:
    void process();
signals:
    void finished();
    void error(QString err);
private:
    std::string source;
    std::string destination;
    QString os;
};

#endif // WORKER_H
