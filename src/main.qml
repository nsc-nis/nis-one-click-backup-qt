import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 2.15
import QtQuick.Dialogs
import QtCore
import at.nis.ocb 1.0
import Qt.labs.folderlistmodel
//import at.nis.ocb.folderList 1.0

Window
{
    id: window_main
    width: 690
    height: 600
    visible: true
    title: qsTr("NIS One-Click-Backup")
    signal startBackup()
    signal sendDestination(string path)
    signal sendAdditional(string path)
    signal defaultFolderChanged(string path, bool checked)
    signal deleteAdditional(string path)
    signal exportBackupPlan(string path)
    signal importBackupPlan(string path)
    property var firstRun;

    Component.onCompleted: firstRun = true;

    Connections
    {
        target: backupManager
        function onBackupManagerError(msg)
        {
            errorDialog.text = msg;
            errorDialog.open();
        }
        function onEnableStartButton()
        {
            button_startBackup.enabled = true;
        }
        function onBackupManagerProgress(progress)
        {
            console.log(progress);
            progressBar.value = progress;

            if(progress === 1)
            {
                label_progressTitle.text = qsTr("Backup finished! You can close this dialog now");
                button_close.enabled = true;
            }
        }
        /* ==> didn't work, QML wouldn't receive the parameters as arrays
        function onInitSettings(destination, defaults, additionals)
        {
            console.log("[ INFO ] Imported parameters: " + destination + ";" + defaults + ";" + additionals);
            textField_destinationPath.text = destination;

            checkbox_pictures.checked = defaults[0];
            checkbox_documents.checked = defaults[1];
            checkbox_downloads.checked = defaults[2];
            checkbox_desktop.checked = defaults[3];
            checkbox_music.checked = defaults[4];
            checkbox_videos.checked = defaults[5];

            for(let folder in additionals)
            {
                model_customFolders.append({folderName: folder});
            }
        }
        */
        function onInitSettings(csv)
        {
            console.log("[ INFO ] Imported parameters: " + csv);

            let parameters = csv.split(";");

            //get destination path
            textField_destinationPath.text = parameters[0];

            //get default folder settings
            let defaults = parameters[1].split(",");

            if(defaults[0] == "true")
            {
                checkbox_pictures.checked = true;
            }
            else
            {
                checkbox_pictures.checked = false;
            }

            if(defaults[1] == "true")
            {
                checkbox_documents.checked = true;
            }
            else
            {
                checkbox_documents.checked = false;
            }

            if(defaults[2] == "true")
            {
                checkbox_downloads.checked = true;
            }
            else
            {
                checkbox_downloads.checked = false;
            }

            if(defaults[3] == "true")
            {
                checkbox_desktop.checked = true;
            }
            else
            {
                checkbox_desktop.checked = false;
            }

            if(defaults[4] == "true")
            {
                checkbox_music.checked = true;
            }
            else
            {
                checkbox_music.checked = false;
            }

            if(defaults[4] == "true")
            {
                checkbox_videos.checked = true;
            }
            else
            {
                checkbox_videos.checked = false;
            }

            //get additional folders
            let additionals = parameters[2].split(",");

            /* => why doesn't this work??
            for(let folder in additionals)
            {
                console.log("[ DEBUG ] Additional: " + folder);

                model_customFolders.append({folderName: folder});
            }
            */
            for(let i = 0; i < additionals.length; i++)
            {
                if(additionals[i] != "")
                {
                    model_customFolders.append({folderName: additionals[i]});
                }
            }
        }
    }

    MessageDialog
    {
        id: errorDialog
        buttons: MessageDialog.Ok
        //onAccepted: document.save()
    }

    /*
      Loader to start other windows
      */
    Loader
    {
            id : loader
    }

    /*
      MenuBar at the top of the screen
      */
    MenuBar
    {
        id: menuBar
        width: parent.width

        Menu
        {
            title: qsTr("File")

            /*
              Saves the current backup-plan, if a before imported one is currently used
              */
            Action
            {
                text: qsTr("Save current backup-plan")
                enabled: false
            }

            /*
              Quits the program
              */
            Action
            {
                text: qsTr("Exit")
                onTriggered: window_main.close()
            }
        }

        Menu
        {
            title: qsTr("Settings")


            /*
              Change Language settings
            */
            Action
            {
                text: qsTr("Language settings")
                onTriggered: {
                    console.log("[ OK ] Language-action triggered")
                    console.log("[ *** ] Launching LanguageWindow")
                    loader.source = "LanguageWindow.qml"
                }
                enabled: false
            }

            /*
              Exports the current backup-configuration into a .JSON-style document
              */
            Action
            {
                text: qsTr("Export backup plan")
                onTriggered: folderDialog_export.open()
            }

            /*
              Imports a previously exported backup-configuration
              */
            Action
            {
                text: qsTr("Import backup plan")
                onTriggered: fileDialog_import.open()
            }
        }
        Menu
        {
            title: qsTr("Help")

            /*
              Opens a help-window where the functions of this program are explained
              */
            Action
            {
                text: qsTr("How to use One-Click-Backup")
                onTriggered: {
                    console.log("[ OK ] HowTo-action triggered")
                    console.log("[ *** ] Launching HelpWindow")
                    loader.source = "HelpWindow.qml"
                }
            }

            /*
              Opens an about-window to display some information about the program
              */
            Action
            {
                text: qsTr("About")
                onTriggered: {
                    console.log("[ OK ] About-action triggered")
                    console.log("[ *** ] Launching AboutWindow")
                    loader.source = "AboutWindow.qml"
                }
            }
        }
    }

    ColumnLayout
    {
        id: layout_root
        width: parent.width
        anchors.top: menuBar.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10

        /*
          "Header-bar": displays the logo and name of the application
          */
        RowLayout
        {
            id: layout_title
            width: parent.width
            Layout.alignment: Qt.AlignHCenter + Qt.AlignTop

            /*
              Logo of the application
              */
            Rectangle
            {
                id: image_logo
                height: 64
                width: 64
                Layout.leftMargin: 10
                Layout.rightMargin: 0
                Layout.alignment: Qt.AlignRight

                Image
                {
                    height: parent.height
                    width: parent.width
                    source: "qrc:/resources/NIS_OCB_Icon_logo.png"
                }
            }

            /*
              Title of the application
              */
            Label
            {
                id: label_title
                text: "NIS One-Click-Backup"
                font.pointSize: 42
                color: "blue"
                Layout.alignment: Qt.AlignLeft
            }
        }

        /*
          Display the controls to configure where to store the backup
          */
        RowLayout
        {
            id: layout_destinationPath
            width: parent.width
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom

            /*
              TextField to display the path to the location the backup is going to be stored
              */
            TextField
            {
                id: textField_destinationPath
                font.pointSize: 12
                Layout.fillWidth: true
                placeholderText: qsTr("Choose a place where you want your backup to be stored")
                placeholderTextColor: "lightgray"
                text: {
                    //var path = myFileDialog.fileUrl.toString();
                    var path = folderDialog_destination.selectedFolder.toString()
                    // remove prefixed "file:///"
                    path = path.replace(/^(file:\/{3})/,"");
                    // unescape html codes like '%23' for '#'
                    var cleanPath = decodeURIComponent(path);
                    text = cleanPath;
                }
                onTextChanged: window_main.sendDestination(text);
            }

            /*
              Opens a folder-chooser to choose the backup-location
              */
            Button
            {
                id: button_pickPath
                font.pointSize: 12
                text: qsTr("Choose Destination")
                icon.source: "qrc:/resources/NIS_OCB_Icon_destination.png"
                icon.color: "transparent"
                onClicked: folderDialog_destination.open()
            }
        }

        /*
          Displays a few standard folders the user can choose to backup
          */
        ColumnLayout
        {
            id: layout_checkboxes
            width: parent.width
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            Label
            {
                id: label_folderTitle
                text: qsTr("User folders to backup")
                //y: layout_title.bottom - 10
                //Layout.topMargin: 5
                Layout.rightMargin: 5
                Layout.bottomMargin: 0
                font.bold: true
            }

            CheckBox
            {
                id: checkbox_pictures
                checked: true
                text: qsTr("Pictures")
                onCheckedChanged: window_main.defaultFolderChanged("Pictures", checked)
            }

            CheckBox
            {
                id: checkbox_documents
                checked: true
                text: qsTr("Documents")
                onCheckedChanged: window_main.defaultFolderChanged("Documents", checked)
            }

            CheckBox
            {
                id: checkbox_downloads
                checked: false
                text: qsTr("Downloads")
                onCheckedChanged: window_main.defaultFolderChanged("Downloads", checked)
            }

            CheckBox
            {
                id: checkbox_desktop
                checked: false
                text: qsTr("Desktop")
                onCheckedChanged: window_main.defaultFolderChanged("Desktop", checked)
            }

            CheckBox
            {
                id: checkbox_music
                checked: true
                text: qsTr("Music")
                onCheckedChanged: window_main.defaultFolderChanged("Music", checked)
            }

            CheckBox
            {
                id: checkbox_videos
                checked: true
                text: qsTr("Videos")
                onCheckedChanged: window_main.defaultFolderChanged("Videos", checked)
            }

            Label
            {
                id: label_additionalFoldersTitle
                text: qsTr("Additional folders to backup")
                Layout.rightMargin: 5
                Layout.bottomMargin: 0
                font.bold: true
            }
        }
    }

    /*
      Displays a list of additional folders the user wants to backup
      */
    Rectangle
    {
        id: container_listView
        width: parent.width
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: layout_root.bottom
        anchors.topMargin: 5
        anchors.bottom: container_startButton.top

        ListModel
        {
            id: model_customFolders
            ListElement
            {
                folderName: ""//qsTr("Choose an additional folder you want to backup")
            }
        }

        ListView
        {
            id: listView
            anchors.fill: parent
            //width: parent.width
            model: model_customFolders
            clip: true
            spacing: 2

            delegate: Rectangle {
                height: 50
                width: parent.width
                //anchors.left: parent.left
                //anchors.right: parent.right
                //anchors.top: parent.top

                /*
                  Clear the current additional folder
                  */
                Button
                {
                    id: button_clearAdditionalPath
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.rightMargin: 5
                    anchors.bottom: textField_extraPath.bottom
                    icon.color: "transparent"
                    icon.source: "qrc:/resources/NIS_OCB_icon_delete.png"
                    //Layout.fillHeight: true
                    onClicked: {
                        //model_customFolders.remove(listView.currentIndex, 1);
                        if(textField_extraPath.text != "")
                        {
                            for(var i = 0; i < model_customFolders.count; i++)
                            {
                                console.log("[ DEBUG ] Index: " + i + "; Model: " + model_customFolders.get(i).folderName + "; TextField: " + textField_extraPath.text);
                                if(model_customFolders.get(i).folderName == textField_extraPath.text)
                                {
                                    model_customFolders.remove(i, 1);
                                    window_main.deleteAdditional(textField_extraPath.text)
                                }
                            }
                        }
                    }
                }

                /*
                  Displays the path of the current additional folder
                  */
                TextField
                {
                    id: textField_extraPath
                    anchors.left: button_clearAdditionalPath.right
                    anchors.leftMargin: 5
                    anchors.right: button_pickPathAdditional.left
                    anchors.rightMargin: 5
                    anchors.top: parent.top
                    font.pointSize: 12
                    placeholderText: qsTr("Choose an additional folder you want to backup")
                    placeholderTextColor: "lightgray"
                    //Layout.fillWidth: true
                    text: folderName
                }

                /*
                  Opens a folder-chooser to choose an additional folder
                  */
                Button
                {
                    id: button_pickPathAdditional
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    anchors.top: parent.top
                    font.pointSize: 12
                    text: qsTr("Choose Folder")
                    icon.source: "qrc:/resources/NIS_OCB_Icon_folder.png"
                    icon.color: "transparent"
                    onClicked: folderDialog_additional.open()
                }

                FolderDialog
                {
                    id: folderDialog_additional
                    currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
                    onAccepted: {
                        var path = folderDialog_additional.selectedFolder.toString();
                        // remove prefixed "file:///"
                        path = path.replace(/^(file:\/{3})/,"");
                        // unescape html codes like '%23' for '#'
                        var cleanPath = decodeURIComponent(path);

                        window_main.sendAdditional(cleanPath);
                        /*
                        textField_extraPath.text = cleanPath;
                        model_customFolders.append({folderName: ""})
                        */

                        model_customFolders.append({folderName: cleanPath})
                        for(var i = 0; i < model_customFolders.count; i++)
                        {
                            if(model_customFolders.get(i).folderName == "")
                            {
                                model_customFolders.move(i, model_customFolders.count - 1, 1)
                            }
                        }
                    }
                }
            }
        }       
    }

    Rectangle
    {
        id: container_startButton
        width: parent.width
        //anchors.bottom: container_progress.top
        //anchors.top: container_listView.bottom
        anchors.bottom: parent.bottom
        color: "#F7F7FA"
        height: 50

        /*
          Button to start the backup-process
          */
        Button
        {
            id: button_startBackup
            height: 40
            enabled: false
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            text: qsTr("Start Backup")
            icon.color: "transparent"
            icon.source: "qrc:/resources/NIS_OCB_icon_next.png"
            onClicked: {
                dialog_progress.open();
                window_main.startBackup();

                //loader.source = "BackupProgress.qml"
            }
        }
    }

    Dialog
    {
        id: dialog_progress
        width: 690
        height: 100
        anchors.centerIn: parent

        Label
        {
            id: label_progressTitle
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            width: parent.width
            //visible: false
            //onVisibleChanged: window_main.startBackup();
            //anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Your backup is in progress, please wait...")
            //text: qsTr("Backup progress:")
        }

        Label
        {
            id: label_progress
            anchors.top: label_progressTitle.bottom
            anchors.left: parent.left
            anchors.leftMargin: 10
            //visible: false
            text: parseInt((progressBar.value * 100), 10) + "% " + qsTr("done")
        }

        ProgressBar
        {
            id: progressBar
            anchors.top: label_progress.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            value: 0
            //visible: false
        }

        Button
        {
            id: button_close
            text: qsTr("Close")
            anchors.top: progressBar.bottom
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            enabled: false
            onClicked: {
                dialog_progress.close()
            }
        }
    }

    FolderDialog
    {
        id: folderDialog_destination
        currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
        onAccepted: {
            //var path = myFileDialog.fileUrl.toString();
            var path = folderDialog_destination.selectedFolder.toString()
            // remove prefixed "file:///"
            path = path.replace(/^(file:\/{3})/,"");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            textField_destinationPath.text = cleanPath
        }
    }

    FolderDialog
    {
        id: folderDialog_export
        currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
        onAccepted: {
            //var path = myFileDialog.fileUrl.toString();
            var path = folderDialog_export.selectedFolder.toString()
            // remove prefixed "file:///"
            path = path.replace(/^(file:\/{3})/,"");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            window_main.exportBackupPlan(cleanPath)
        }
    }

    FileDialog
    {
        id: fileDialog_import
        currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
        nameFilters: ["NIS config files (*.nscf)"]
        onAccepted: {
            var path = fileDialog_import.selectedFile.toString()
            // remove prefixed "file:///"
            path = path.replace(/^(file:\/{3})/,"");
            // unescape html codes like '%23' for '#'
            var cleanPath = decodeURIComponent(path);
            window_main.importBackupPlan(cleanPath)
        }
    }
}
