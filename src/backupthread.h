#ifndef BACKUPTHREAD_H
#define BACKUPTHREAD_H

#include <QThread>
#include <filesystem>
#include <QDebug>

class BackupThread : public QThread
{
    Q_OBJECT
private:
    std::string source;
    std::string destination;

public:
    BackupThread(std::string, std::string);
    void run() override;

signals:
    void resultReady();
};

#endif // BACKUPTHREAD_H
