#!/bin/bash

green=$'\e[1;32m'
yellow=$'\e[1;33m'
red=$'\e[1;31m'

qtpath=~/Qt/6.6.1/gcc_64/

echo "$yellow (i) [ NIS One-Click-Backup Buildsystem v. 1.0 ]"
echo "$yellow (i) [ Building the app using CMake ]"
echo "$yellow -----------------------------------------------"

if [ "$1" != "" ]; then
    echo "$green [ OK ] Custom Qt-Path specified: " $1
    qtpath=$1
else
    echo "$red *** No custom Qt-installation path specified ***"
    echo "$red Using default path: " $qtpath
fi

#export correct path to Qt installation
echo "$yellow (i) Exporting CMAKE_PREFIX_PATH..."
export CMAKE_PREFIX_PATH=$qtpath
echo "$green [ OK ] Exported CMAKE_PREFIX_PATH: " $CMAKE_PREFIX_PATH

#cleaning up (possible) previous build directory
echo "$yellow (i) Cleaning up (possible) previous build directory..."
rm -r build-cmake
echo "$green [ OK ] Cleaned up build directory"

#create build directory
echo "$yellow (i) Creating build directory..."
mkdir build-cmake
cd build-cmake
echo "$green [ OK ] Created build directory"

#execute CMake with Ninja as the generator
echo "$yellow (i) Running CMake with Ninja..."
cmake .. -G Ninja

#build the application with Ninja
ninja

echo "$green [ OK ] Buildscript finished"

exit
