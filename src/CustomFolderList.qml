import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 2.15

Item
{
    id: folderList

    RowLayout
    {
        width: parent.width
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        /*
        Layout.leftMargin: 10
        Layout.rightMargin: 10
        Layout.fillWidth: true
        */

        Button
        {
            id: button_clearAdditionalPath
            font.pointSize: 12
            icon.source: "qrc:/images/NIS_OCB_icon_delete.png"
            //Layout.leftMargin: 10
            //width: 10
        }

        TextField
        {
            id: textField_extraPath
            font.pointSize: 12
            placeholderText: qsTr("Choose an additional folder you want to backup")
            placeholderTextColor: "lightgray"
        }

        Button
        {
            id: button_pickPathAdditional
            font.pointSize: 12
            text: qsTr("Choose Folder")
            icon.source: "qrc:/images/NIS_OCB_Icon_folder.png"
            Layout.leftMargin: 0
            Layout.rightMargin: 10
        }
    }
}

