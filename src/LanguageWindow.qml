import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

/*
  Show a window that allows the user to switch languages
*/
Window
{
    id: window_languageWindow
    width: 400
    height: 310
    title: qsTr("Change user interface language")
    visible: true

    /*
      "Header-bar": displays the logo and name of the current window
      */
    RowLayout
    {
        id: layout_title
        width: parent.width
        Layout.alignment: Qt.AlignHCenter + Qt.AlignTop

        /*
          Logo of the application
          */
        Rectangle
        {
            id: image_logo
            height: 64
            width: 64
            Layout.leftMargin: 10
            Layout.rightMargin: 0
            Layout.alignment: Qt.AlignRight

            Image
            {
                height: parent.height
                width: parent.width
                source: "qrc:/resources/NIS_OCB_Icon_language.png"
            }
        }

        /*
          Title of the application
          */
        Label
        {
            id: label_title
            text: "Language"
            font.pointSize: 42
            color: "blue"
            Layout.alignment: Qt.AlignLeft
        }

    }
    /*
    ListModel
    {
        id: model_languageChooser
        ListElement
        {
            //TODO
        }
    }
    */

    ListView
    {
        anchors.top: layout_title.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        width: parent.width
        model: 2
        delegate: RadioButton {
            //text: TODO
        }
    }
}
