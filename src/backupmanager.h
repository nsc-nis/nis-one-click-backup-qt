#ifndef BACKUPMANAGER_H
#define BACKUPMANAGER_H

#include <QObject>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickView>
#include <QQmlContext>
#include <QStandardPaths>
#include <QFile>
#include "backupplan.h"

class BackupManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString destination READ getDestination WRITE setDestination) // NOTIFY destinationChanged)

public:
    explicit BackupManager(QObject *parent = nullptr);
    //~BackupManager();
    void setDestination(QString);
    QString getDestination();

signals:
    void backupManagerError(QString backupErrorMsg);
    void backupManagerProgress(double progress);
    void backupStarted();
    void enableStartButton();
    void initSettings(QString);

private slots:
    void startBackup();
    void receiveDestination(QString);
    void receiveAdditional(QString);
    void onDefaultFolderChanged(QString, bool);
    void onBackuProgressChanged(double);
    void onAdditionalFolderDeleted(QString);
    void onErrorMessage(QString);
    void exportPlan(QString);
    void importPlan(QString);
    //void getDestination(QString);

private:
   QString os;
   QString username;
   QString destinationPath;
   BackupPlan backupPlan;
};

#endif // BACKUPMANAGER_H
