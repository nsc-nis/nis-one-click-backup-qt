#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QLocale>
#include <QTranslator>
#include <QQmlContext>
#include "backupmanager.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "NIS_One-Click-Backup_Qt_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QQmlApplicationEngine engine;
    qmlRegisterType<BackupManager>("at.nis.ocb", 1, 0, "BackupManager");

    BackupManager manager;
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("backupManager", &manager);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    /*
    QQmlApplicationEngine engineProgress;
    QQmlContext* contextProgress = engineProgress.rootContext();
    contextProgress->setContextProperty("backupManager", &manager);

    const QUrl urlProgress(QStringLiteral("qrc:/BackupProgress.qml"));
    engineProgress.load(urlProgress);
    */

    QObject* item = engine.rootObjects().value(0);
    QObject::connect(item, SIGNAL(startBackup()), &manager, SLOT(startBackup()));
    QObject::connect(item, SIGNAL(sendDestination(QString)), &manager, SLOT(receiveDestination(QString)));
    QObject::connect(item, SIGNAL(sendAdditional(QString)), &manager, SLOT(receiveAdditional(QString)));
    QObject::connect(item, SIGNAL(defaultFolderChanged(QString, bool)), &manager, SLOT(onDefaultFolderChanged(QString, bool)));
    QObject::connect(item, SIGNAL(deleteAdditional(QString)), &manager, SLOT(onAdditionalFolderDeleted(QString)));
    QObject::connect(item, SIGNAL(exportBackupPlan(QString)), &manager, SLOT(exportPlan(QString)));
    QObject::connect(item, SIGNAL(importBackupPlan(QString)), &manager, SLOT(importPlan(QString)));
    //QObject::connect(&manager, SIGNAL(backupManagerProgress(double)), item, SLOT(onDefaultFolderChanged(QString, bool)));

    return app.exec();
}
