#!/bin/bash
cd ../../src
rm -r build-appimage
mkdir build-appimage
cd build-appimage

~/Qt/6.3.0/gcc_64/bin/qmake ..

make -j$(nproc)

#make install INSTALL_ROOT=AppDir
#cp NIS_One-Click-Backup_Qt ../../packaging/AppImage/AppDir/usr/bin

cd ../../packaging/AppImage/

wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
wget https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage
chmod +x linuxdeploy*.AppImage

export QML_SOURCES_PATHS=../../src
export QMAKE=~/Qt/6.3.0/gcc_64/bin/qmake
export LD_LIBRARY_PATH=/usr/lib:~/Qt/6.3.0/gcc/lib

#run linuxdeploy and generate an AppDir
./linuxdeploy-x86_64.AppImage --appdir AppDir --executable ../../src/build-appimage/NIS_One-Click-Backup_Qt --desktop-file one-click-backup.desktop --output appimage --plugin qt #--icon-file icons/hicolor/64x64/apps/NIS_One-Click-Backup.png
rm linuxdeploy*.AppImage
