;Change this file to customize zip2exe generated installers with a modern interface
# copy this file to C:\Program Files (x86)\NSIS\Contrib\zip2exe

!include "MUI2.nsh"
!include "NTProfiles.nsh"

RequestExecutionLevel admin

Var StartMenuFolder

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\win.bmp" ; optional
!define MUI_ABORTWARNING

!define MUI_HEADERIMAGE_UNBITMAP "${NSISDIR}\Contrib\Graphics\Header\win.bmp" ; optional

!define MUI_ICON "C:\Users\vm\Documents\nis-one-click-backup-qt\src\resources\NIS_OCB_Icon_logo.ico"
!define MUI_UNICON "C:\Users\vm\Documents\nis-one-click-backup-qt\src\resources\NIS_OCB_Icon_logo.ico"

!insertmacro MUI_PAGE_DIRECTORY

!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\NIS One-Click-Backup" 
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

Section "Install"
	SetOutPath $INSTDIR
	
	# Create uninstaller
	WriteUninstaller "$INSTDIR\Uninstall.exe"
	
	# MessageBox MB_OK|MB_ICONINFORMATION 'Installation directory: $INSTDIR'

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
						"DisplayName" "NIS One-Click-Backup"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
						"DisplayIcon" "$INSTDIR\resources\NIS_OCB_Icon_logo.ico"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
						"Publisher" "NSC IT Solutions (NIS)"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
						"DisplayVersion" "1.2"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
						"UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup" \
					 "QuietUninstallString" "$\"$INSTDIR\Uninstall.exe$\" /S"
					 				 
	  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
		# Create shortcuts
		# CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
		# CreateShortcut "$SMPROGRAMS\$StartMenuFolder\NIS One-Click-Backup.lnk" "$INSTDIR\NIS_One-Click-Backup_Qt.exe"
		# CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall NIS One-Click-Backup.lnk" "$INSTDIR\Uninstall.exe"
		#CreateShortcut "$SMPROGRAMS\NIS One-Click-Backup.lnk" "$INSTDIR\NIS_One-Click-Backup_Qt.exe"
		SetShellVarContext all
		CreateShortcut "$desktop\NIS One-Click-Backup.lnk" "$INSTDIR\NIS_One-Click-Backup_Qt.exe"
		#CreateShortcut "${ProfilePathAllUsers}\NIS One-Click-Backup.lnk" "$INSTDIR\NIS_One-Click-Backup_Qt.exe"
		CreateShortcut "$SMPROGRAMS\NIS One-Click-Backup.lnk" "$INSTDIR\NIS_One-Click-Backup_Qt.exe"
	 !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section "Uninstall"
	SetShellVarContext all
	Delete "$INSTDIR\Uninstall.exe"

	RMDir /R /REBOOTOK "$INSTDIR"
	#Delete "$INSTDIR"
  
	# !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
	# Delete "$SMPROGRAMS\$StartMenuFolder\NIS_One-Click-Backup_Qt.lnk"
	Delete "$SMPROGRAMS\NIS One-Click-Backup.lnk"
	Delete "$desktop\NIS One-Click-Backup.lnk"

	RMDir "$SMPROGRAMS\$StartMenuFolder"
	
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NIS_One-Click-Backup"
SectionEnd
