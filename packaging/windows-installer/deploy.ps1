# Script to deploy NIS One-Click-Backup on Windows
# pass directory to qml and to app-binary to the script as an argument
param([string]$qmldir="C:\deploy\qmldir", [string]$deploydir="C:\deploy") 

# assuming the standard Qt installation directory
C:\Qt\6.5.1\mingw_64\bin\windeployqt.exe --qmldir $qmldir "$deploydir\NIS_One-Click-Backup_Qt.exe"