#!/bin/bash
green=$'\e[1;32m'
yellow=$'\e[1;33m'
red=$'\e[1;31m'

qtpath=~/Qt/6.6.1/gcc_64/

echo "$yellow (i) [ NIS One-Click-Backup Buildsystem v. 1.0 ]"
echo "$yellow (i) [ Building the app using CMake ]"
echo "$yellow -----------------------------------------------"

if [ "$2" != "" ]; then
    echo "$green [ OK ] Custom Qt-Path specified: " $1
    qtpath=$1
else
    echo "$red *** No custom Qt-installation path specified ***"
    echo "$red Using default path: " $qtpath
fi

#export correct path to Qt installation
echo "$yellow (i) Exporting CMAKE_PREFIX_PATH..."
export CMAKE_PREFIX_PATH=$qtpath
echo "$green [ OK ] Exported CMAKE_PREFIX_PATH: " $CMAKE_PREFIX_PATH

#Clean up previous directory
rm -r build-flatpak

if [ "$1" == "install" ]; then
    ## Install the flatpak
    flatpak-builder --user --install --force-clean build-flatpak io.gitlab.dev_nis.one-click-backup.yml
elif [ "$1" == "build" ]; then
    ## Build the Flatpak
    flatpak-builder build-flatpak io.gitlab.dev_nis.one-click-backup.yml
elif [ "$1" == "repo" ]; then
    flatpak-builder --repo=io.gitlab.dev_nis.one-click-backup --force-clean build-flatpak io.gitlab.dev_nis.one-click-backup.yml
elif [ "$1" == "export" ]; then
    # Build
    flatpak-builder build-flatpak/ io.gitlab.dev_nis.one-click-backup.yml

    #Export
    flatpak build-export export build-flatpak

    #Export to file
    flatpak build-bundle export io.gitlab.dev_nis.one-click-backup.flatpak io.gitlab.dev_nis.one-click-backup --runtime-repo=https://flathub.org/repo/flathub.flatpakrepo
else
    echo "[ ERROR ] No command specified. Use one of these:"
    echo "           install    :   Install the flatpak"
    echo "           build      :   Build the flatpak"
    echo "           repo       :   Put the flatpak in a repo"
    echo "           export     :   Export the flatpak"
fi
