# NIS One-Click-Backup (Qt Version)
A Simple Program to backup folders to an external location by copying them. 
This program allows you to copy multiple folders to another location with just one click. By default, you can select every folder in your User-directory (on Windows) or your Home-directory (on Linux).
In addition to that, it is also possible to add as many custom folders to the program as you want. After you have made all of the settings, just click the "Start"-Button and it will start copying your folders. 

## Qt-Version
This is the "platform-independent"-Version written in Qt(QML).  
## Download 
### Windows

- **Install with [WINGET Package Manager](https://learn.microsoft.com/en-us/windows/package-manager/winget/):** \
  `winget install one-click-backup`
- **Download installer executable**:\
  An installer-executable can be downloaded on the [releases page](https://gitlab.com/dev-nis/nis-one-click-backup-qt/-/releases) of this repository.

### Linux
- **Download AppImage**:\
  An AppImage for every Linux distro (that supports AppImages) can be downloaded on the [releases page](https://gitlab.com/dev-nis/nis-one-click-backup-qt/-/releases) of this repository.
- **Flatpak**:\
  <a href='https://flathub.org/apps/io.gitlab.dev_nis.one-click-backup'>
    <img width='140' alt='Download on Flathub' src='https://flathub.org/api/badge?locale=en'/>
  </a><br>
  A Flatpak of this version is available on [Flathub](https://flathub.org/apps/io.gitlab.dev_nis.one-click-backup). It can be installed with your distro's software center (like GNOME Software or KDE Discover) or by executing in a terminal `flatpak install flathub io.gitlab.dev_nis.one-click-backup`
- **Nix packages**:\
  This application can also be installed via the [Nix package manager](https://nixos.org/) (e.g. on NixOS). 
  - NixOS config (`/etc/nixos/configuration.nix`):
  ```
  environment.systemPackages = [
    pkgs.one-click-backup
  ];
  ```
  - nix-shell:
  `nix-shell -p one-click-backup`
  - nix-env: \
  On NixOS: `nix-env -iA nixos.one-click-backup` \
  On non-NixOS: `nix-env -iA nixpkgs.one-click-backup` \
  On non-NixOS with flakes: `nix profile install nixpkgs#one-click-backup`

In the future, it is planned to publish the application to other various packaging formats like Chocolatey, Snap and maybe also for various Linux-distros' packaging formats. ~~A version for macOS is also planned~~ _Note: With the progressing move of macOS towards Apple Silicon (which I'm not able to build on) I will not port my application to macOS since I think it doesn't make sense to support a soon obsolete platform (macOS on x86)_

## Screenshots
Main page: </br>
<img src="https://gitlab.com/dev-nis/nis-one-click-backup-qt/-/raw/d8a1f26d7d1a678f84ce51c7f8b2cb21de16ff56/screenshots/main-page.png?inline=false" align="center"/>
